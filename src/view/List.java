package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JLabel;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import java.sql.*;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.JScrollPane;

import oracle.jdbc.driver.DBConversion;

public class List extends MainGui {

	private String name;
	private JPanel contentPane;
	private JTable table;

	/**
	 * Create the frame.
	 */
	public List(String name) {
		Connection connection;
		this.name = name;
		frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBounds(100, 100, 450, 300);

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		frame.setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel(name.toUpperCase());
		lblNewLabel.setBounds(71, 26, 70, 15);
		contentPane.add(lblNewLabel);

		JButton btnZpet = new JButton("Zpet");
		btnZpet.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
				Window menu = new Window();
				menu.getFrame().setVisible(true);
			}
		});
		btnZpet.setBounds(24, 246, 117, 25);
		contentPane.add(btnZpet);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(24, 80, 414, 154);
		contentPane.add(scrollPane);

		table = new JTable();
		
		try {
			String query = "";
			if (this.name == "skicaky") query = "SELECT \"sortiment\".*, \"" + this.name + "\".\"gramaz\", \"" + this.name + "\".\"velikst\"  FROM \"" + this.name + "\" JOIN \"sortiment\" ON \"" + this.name + "\".\"sortiment_id\" = \"sortiment\".\"sortiment_id\"";
			else if (this.name == "farbicky") query = "SELECT \"sortiment\".*, \"" + this.name + "\".\"dlzka\", \"" + this.name + "\".\"vlastnost\"  FROM \"" + this.name + "\" JOIN \"sortiment\" ON \"" + this.name + "\".\"sortiment_id\" = \"sortiment\".\"sortiment_id\"";
			else query = "SELECT * FROM \"" + this.name + "\"";
			connection = getConnection();
			PreparedStatement pst = connection.prepareStatement(query);
			ResultSet rs = pst.executeQuery();

			table.setModel(this.buildTableMode(rs));
			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		scrollPane.setViewportView(table);
		
		
	}

	public DefaultTableModel buildTableMode(ResultSet rs) throws SQLException {
		ResultSetMetaData meta = rs.getMetaData();

		Vector<String> colNames = new Vector<String>();
		int colCount = meta.getColumnCount();
		for (int i = 1; i <= colCount; i++) {
			colNames.add(meta.getColumnName(i));
		}

		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		while (rs.next()) {
			Vector<Object> row = new Vector<Object>();
			for (int i = 1; i <= colCount; i++) {
				row.add(rs.getObject(i));
			}
			data.add(row);
		}
		
		return (new DefaultTableModel(data, colNames));
	}
}
