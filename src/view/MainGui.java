package view;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.swing.JFrame;

public class MainGui {
	protected JFrame frame;
	protected Connection connection;

	public JFrame getFrame() {
		return (frame);
	}

	public void setConnection(Connection con) {
		this.connection = con;
	}
	
	public Connection getConnection() throws SQLException{
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver").newInstance();
		} catch (ClassNotFoundException e) {
			System.out.println("ERROR: Load driver class");
			System.exit(1);
		} catch (IllegalAccessException e) {
			System.out.println("ERROR: access problem while loading");
			System.exit(1);
		} catch (InstantiationException e) {
			System.out.println("ERROR: unable to instantiate driver");
			System.exit(1);
		}
		
		Driver driver = new oracle.jdbc.driver.OracleDriver();
		DriverManager.registerDriver(driver);
		
		String url = "jdbc:oracle:thin:@gort.fit.vutbr.cz:1521:dbgort";
		String user = "xcerny63";
		String pass = "hoibij63";
		try {
			connection = DriverManager.getConnection(url, user, pass);
		} catch (SQLException e) {
			System.out.println("Connection Failed");
			e.printStackTrace();
			System.exit(1);
		}
		return (connection);
	}
}
