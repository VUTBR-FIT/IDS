package view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.sql.SQLException;

import javax.swing.JLabel;


public class Window extends MainGui {

	private JPanel contentPane;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Window window = new Window();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Window() {
		this.frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBounds(100, 100, 450, 300);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		frame.setContentPane(contentPane);
		contentPane.setLayout(null);
		
/*		JButton btnZakaznikAdd = new JButton("ADD");
		btnZakaznikAdd.setFont(new Font("Dialog", Font.BOLD, 10));
		btnZakaznikAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnZakaznikAdd.setBounds(197, 45, 59, 25);
		contentPane.add(btnZakaznikAdd);*/
		
/*		JButton btnDodavatelAdd = new JButton("ADD");
		btnDodavatelAdd.setFont(new Font("Dialog", Font.BOLD, 10));
		btnDodavatelAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnDodavatelAdd.setBounds(197, 72, 59, 25);
		contentPane.add(btnDodavatelAdd);*/
		
/*		JButton btnSkicakAdd = new JButton("ADD");
		btnSkicakAdd.setFont(new Font("Dialog", Font.BOLD, 10));
		btnSkicakAdd.setBounds(197, 126, 59, 25);
		contentPane.add(btnSkicakAdd);
		
		JButton btnPastelkyAdd = new JButton("ADD");
		btnPastelkyAdd.setFont(new Font("Dialog", Font.BOLD, 10));
		btnPastelkyAdd.setBounds(197, 153, 59, 25);
		contentPane.add(btnPastelkyAdd);*/
		
		JButton btnZamestnanecList = new JButton("Vypsat");
		btnZamestnanecList.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
				List list = new List("zamestnanec");
				list.getFrame().setVisible(true);
			}
		});
		btnZamestnanecList.setBounds(268, 98, 83, 25);
		contentPane.add(btnZamestnanecList);
		
		JLabel lblZakaznik = new JLabel("Zakaznik");
		lblZakaznik.setBounds(125, 49, 70, 15);
		contentPane.add(lblZakaznik);
		
		JButton btnZakaznikList = new JButton("Vypsat");
		btnZakaznikList.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
				List list = new List("zakaznik");
				list.getFrame().setVisible(true);
			}
		});
		btnZakaznikList.setBounds(268, 44, 83, 25);
		contentPane.add(btnZakaznikList);
		
		JLabel lblDodavatel = new JLabel("Dodavatel");
		lblDodavatel.setBounds(118, 76, 77, 15);
		contentPane.add(lblDodavatel);
		
		JButton btnDodavatelList = new JButton("Vypsat");
		btnDodavatelList.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
				List list = new List("dodavatel");
				list.getFrame().setVisible(true);
			}
		});
		btnDodavatelList.setBounds(268, 71, 83, 25);
		contentPane.add(btnDodavatelList);
		
		JLabel lblZamestnanec = new JLabel("Zamestnanec");
		lblZamestnanec.setBounds(85, 103, 110, 15);
		contentPane.add(lblZamestnanec);
		
/*		JButton btnZamestnanecAdd = new JButton("ADD");
		btnZamestnanecAdd.setFont(new Font("Dialog", Font.BOLD, 10));
		btnZamestnanecAdd.setBounds(197, 98, 59, 25);
		contentPane.add(btnZamestnanecAdd);*/
		
		JLabel lblSkicak = new JLabel("Skicak");
		lblSkicak.setBounds(136, 130, 52, 15);
		contentPane.add(lblSkicak);
		
		JButton btnSkicakList = new JButton("Vypsat");
		btnSkicakList.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
				List list = new List("skicaky");
				list.getFrame().setVisible(true);
			}
		});
		btnSkicakList.setBounds(268, 125, 83, 25);
		contentPane.add(btnSkicakList);
		
		JLabel lblPastelky = new JLabel("Farbicky");
		lblPastelky.setBounds(125, 157, 70, 15);
		contentPane.add(lblPastelky);
		
		JButton btnPastelkyList = new JButton("Vypsat");
		btnPastelkyList.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
				List list = new List("farbicky");
				list.getFrame().setVisible(true);
			}
		});
		btnPastelkyList.setBounds(268, 152, 83, 25);
		contentPane.add(btnPastelkyList);
		
		JLabel lblRecenze = new JLabel("Recenze");
		lblRecenze.setBounds(125, 184, 70, 15);
		contentPane.add(lblRecenze);
		
/*		JButton btnRecenzeAdd = new JButton("ADD");
		btnRecenzeAdd.setFont(new Font("Dialog", Font.BOLD, 10));
		btnRecenzeAdd.setBounds(197, 180, 59, 25);
		contentPane.add(btnRecenzeAdd);*/
		
		JButton btnRecenzeList = new JButton("Vypsat");
		btnRecenzeList.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
				List list = new List("recenzia");
				list.getFrame().setVisible(true);
			}
		});
		btnRecenzeList.setBounds(268, 179, 83, 25);
		contentPane.add(btnRecenzeList);
		
		JLabel lblObjednavka = new JLabel("Objednavka");
		lblObjednavka.setBounds(104, 211, 91, 15);
		contentPane.add(lblObjednavka);
		
/*		JButton btnObjednavkaAdd = new JButton("ADD");
		btnObjednavkaAdd.setFont(new Font("Dialog", Font.BOLD, 10));
		btnObjednavkaAdd.setBounds(197, 206, 59, 25);
		contentPane.add(btnObjednavkaAdd);*/
		
		JButton btnObjednavkaList = new JButton("Vypsat");
		btnObjednavkaList.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
				List list = new List("objednavka");
				list.getFrame().setVisible(true);
			}
		});
		btnObjednavkaList.setBounds(268, 206, 83, 25);
		contentPane.add(btnObjednavkaList);
		
		JButton btnExit = new JButton("EXIT");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				System.exit(0);
			}
		});
		btnExit.setBounds(158, 238, 117, 25);
		contentPane.add(btnExit);
	}
}
